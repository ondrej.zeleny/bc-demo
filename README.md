# Demonstrační aplikace
> Demonstrační aplikace k bakalářské práci Porovnání vlastností webových rámců a ukázková aplikace.

## Spuštění aplikace
Nejjednodušší způsob, jak spustit aplikace je pomocí nástroje live-server z příkazové řádky. Ten si lze stáhnout pomocí balíčkovacího nástroje NPM příkazem.

```sh
npm install -g live-server
```

Dalším krokem je navigace do kořenového adresáře celého projektu a zadání následujícího příkazu, který spustí lokální server.

```sh
live-server --port=3000
```

## Struktura projektu
Kořenová složka projektu obsahuje čtyři podsložky (každá pro jeden rámec).

Nezkompilované Sass a JavaScriptové soubory jsou umístěné ve složce konkrétního frameworku, v podsložce src a zkompilované v podsložce dist.

## Kompilace
Tento krok není nutný, soubory jsou již v základu předkompilované. 

V případě zájmu je možné je zkompilovat takto:

1. Navigace do podsložky konkrétního rámce.
2. Spuštění příkazu pro instalaci balíčků:
```sh
npm install
```
3. Samotná kompilace pomocí příkazu:
```sh
webpack-cli
```

### Poznámka:

Kvůli nové verzi jQuery 3.5.0, nefunguje po kompilaci v Bootstrapu akoreon. Lze to opravit tímto způsobem.

V Bootstrap aplikaci ve složce __node_modules/bootsrap/dist/js/bootstrap.js__ (řádek 1509) změnit:
```sh
if (!data && _config.toggle && /show|hide/.test(config))
```
na
```sh
if (!data && _config.toggle && /show|hide/.test(_config))
```

## Meta
Ondřej Zelený, 2020