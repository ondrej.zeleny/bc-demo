/**
 * Copyright (c) 2020
 *
 * Helper functions for Foundation demo app
 *
 * @author Ondrej Zeleny <ondrej.zeleny@live.com>
 */

 
/** Require libs */ 
const $ = require('jquery');

/**
 * Converts data from form to object.
 * @param {array} data - Takes data from form.
 * @return {object} - Returns object with data.
 */
var toObject = function(input) {
    var output = {};
    input.forEach(function filter(value, index, array) {
        output[value.name] = value.value;
    })

    return output;
}

/**
 * Converts number on two digits.
 * @param {int} number - Takes number.
 * @return {string} - No return value.
 */
var twoDigits = function (number) {
    return (number < 10 ? '0' : '') + number;
}

/**
 * Converts seconds to time object.
 * @param {int} sec - Takes seconds.
 * @return {object} - Return object in format {h: hours, m: minutes, s: seconds}.
 */
var toTime = function (sec) {
    var hours = Math.floor(sec / 1000 / 60 / 60);
    sec -= hours * 1000 * 60 * 60;
    var minutes = Math.floor(sec / 1000 / 60);
    sec -= minutes * 1000 * 60;
    var seconds = Math.floor(sec / 1000);
    sec -= seconds * 1000;

    return {h: hours, m: minutes, s: seconds};
}


/** Export functions */ 
exports.toObject = toObject;
exports.twoDigits = twoDigits;
exports.toTime = toTime;