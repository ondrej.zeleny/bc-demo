/**
 * Copyright (c) 2020
 *
 * Parse templates for Semantic UI demo app
 *
 * @author Ondrej Zeleny <ondrej.zeleny@live.com>
 */


/** Require libs */
const { Liquid } = require('liquidjs');
const events = require('./events.js');
const helpers = require('./helpers.js');

/** Set workspace */
const liquid = new Liquid();
const template = document.querySelector('[type="text/template"]')
const render = document.querySelector('#render')

/**
 * Parse LiquidJS template to HTML.
 * @param {object} db - Takes database object.
 * @param {object} special - Takes object with special data (optional).
 * @return - No return value.
 */
var parseTemplate = function (db, special = undefined) {

    // Load data from DB
    var data = db.value();

    // Check project detail page
    if (template.hasAttribute("data-detail")) {
        const url = new URLSearchParams(window.location.search);
        const project = url.get('project');
        data.actualProject = parseInt(project);
    }

    // Load special parsing data
    if (special !== undefined) {
        data.special = special;
    } else {
        delete data.special;
    }

    // Filter for time difference between two times
    liquid
        .registerFilter('timeDiff', function(start, end) { 
            var diff = new Date(end).getTime() - new Date(start).getTime();
            var time = helpers.toTime(diff);
            return (helpers.twoDigits(time.h) + ":" + helpers.twoDigits(time.m) + ":" + helpers.twoDigits(time.s));
        });

    // Filter for time difference between now and given time
    liquid
        .registerFilter('timeDiffNow', function(start) { 
            var diff = new Date(Date.now()).getTime() - new Date(start).getTime();
            var time = helpers.toTime(diff);
            return (helpers.twoDigits(time.h) + ":" + helpers.twoDigits(time.m) + ":" + helpers.twoDigits(time.s));
        });

    // Filter for count total time
    liquid.registerFilter('totalTime', function(entries) {
        var diff = 0;

        entries.forEach((entry, index) => {
            diff += new Date(entry.endTime).getTime() - new Date(entry.startTime).getTime();
        });

        var time = helpers.toTime(diff);
        return (helpers.twoDigits(time.h) + ":" + helpers.twoDigits(time.m)+ ":" + helpers.twoDigits(time.s));
    })

    // Render template
    liquid
        .parseAndRender(template.innerHTML, data)
        .then(html => render.innerHTML = html)

    // Bind events on DOM
    events.bindEvents(db);
}


/** Export functions */
exports.parseTemplate = parseTemplate;