/**
 * Copyright (c) 2020
 *
 * Main app file for Bulma demo app
 *
 * @author Ondrej Zeleny <ondrej.zeleny@live.com>
 */


/** Require libs */
const $ = require('jquery');
const low = require('lowdb')
const LocalStorage = require('lowdb/adapters/LocalStorage')
const _ = require('lodash');
const tooltip = require('bulma-tooltip');

/** Set workspace */
const adapter = new LocalStorage('db');
const db = low(adapter);

/** Require modules */
const parser = require('./parser.js');
const events = require('./events.js');

/** Prepare database defaults */
db.defaults({ 
    users: [], 
    roles: [],
    teams: [],
    clients: [],
    projects: [],
    tasks: [],
    entries: [],
    timers: [],
    options: {},

    usersCount: 5,
    rolesCount: 3,
    teamsCount: 4,
    clientsCount: 4,
    projectsCount: 5,
    tasksCount: 8,
    entriesCount: 10,
    timersCount: 1,
}).write();


/** Render template with data */
parser.parseTemplate(db);