const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  mode: "development",
  entry: ["./src/js/app.js", "./src/scss/main.scss"],
  output: {
    filename: "js/app.js"
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "css/main.css"
    }),
  ],
  module: {
    rules: [
      {
        test: /\.s(a|c)ss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader
          },
          "css-loader",
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: true,
              config: {
                path: 'postcss.config.js'
              }
            }
          },
          "sass-loader"
        ]
      },
      {
        test: /\.css$/,
        use: [
            'style-loader',                    
            'css-loader'
        ]
      },
      {
        test: /.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: 'fonts/',    // where the fonts will go
            publicPath: '../'       // override the default path
          }
        }]
      }
    ]
  },
  resolve: {
    alias: {
        vue: 'vue/dist/vue.js'
    },
  }
};