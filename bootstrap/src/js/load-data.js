/**
 * Copyright (c) 2020
 *
 * Load sample data for Foundation demo app
 *
 * @author Ondrej Zeleny <ondrej.zeleny@live.com>
 */


/**
 * Load TEST data
 * @param {object} db - Takes database object.
 * @return - No return value.
 */
var loadData = function (db) {
    // Clear data
    clearData(db);

    // Add test data
    db.get('users')
        .push({ id: 1, username: "ondra", name: "Ondřej Červený", roleId: 1, running: 0 })
        .push({ id: 2, username: "honza", name: "Jan Nový", roleId: 2, running: 0 })
        .push({ id: 3, username: "pepa", name: "Josef Blaha", roleId: 2, running: 0 })
        .push({ id: 4, username: "radim", name: "Radim Fiala", roleId: 2, running: 0 })
        .push({ id: 5, username: "jarda", name: "Jaroslav Pecka", roleId: 2, running: 0 })
        .write();

    db.get('teams')
        .push({ id: 1, name: "Core tým", users: [ 1, 2, 3, 4, 5 ], active: 1 })
        .push({ id: 2, name: "Developeři 1", users: [ 2, 3 ], active: 1 })
        .push({ id: 3, name: "Developeři 2", users: [ 4, 5 ], active: 1 })
        .push({ id: 4, name: "Ostatní", users: [ 1 ], active: 0 })
        .write();

    db.get('clients')
        .push({ id: 1, name: "Pear Inc.", desc: "Společnost vyrábějicí telefony, tablety a notebooky.", active: 1 })
        .push({ id: 2, name: "Steel and co.", desc: "Česká strojírenská společnost sídlící v Brně,", active: 1 })
        .push({ id: 3, name: "Nakupto.cz", desc: "Eshop prodávacící všemožné dárky.", active: 1 })
        .push({ id: 4, name: "Old company", desc: "Společnost vyrábějící skotskou whiskey.", active: 0 })
        .write();

    db.get('projects')
        // Active
        .push({ id: 1, name: "Systém pro správu objednávek", clientId: 1, teamId: 1, active: 1 })
        .push({ id: 2, name: "CRM systém", clientId: 2, teamId: 2, active: 1 })
        .push({ id: 3, name: "Nový eshop", clientId: 3, teamId: 3, active: 1 }) 

        // Inactive
        .push({ id: 4, name: "Interní systém", clientId: 1, teamId: 2, active: 0 })
        .push({ id: 5, name: "Správa skladu", clientId: 4, teamId: 3, active: 0 })
        .write();

    db.get('tasks')
        // Project 1
        .push({ id: 1, name: "Štítky k objednávkám", desc: "Roztřidit objednávky na aktivní a neaktivní.", status: 0, userId: 1, projectId: 1 })
        .push({ id: 2, name: "Rozšíření skladu", desc: "Přidat do skladu sloupec měna.", status: 0, userId: 2, projectId: 1 })
        .push({ id: 3, name: "Design login stránky", desc: "Sjednocení a vylepšení designu login stránky.", status: 0, userId: 4, projectId: 1 })

        // Project 2
        .push({ id: 4, name: "Zrušení sekce produktů", desc: "Je potřeba zrušit sekci produkty.", status: 0, userId: 2, projectId: 2 })
        .push({ id: 5, name: "Přidání oprávnění", desc: "Rozsířít opravnění pro managera.", status: 0, userId: 2, projectId: 2 })
        .push({ id: 6, name: "Nová rola", desc: "Přidat roli guest s omezenými právy.", status: 0, userId: 3, projectId: 2 })

        // Project 3
        .push({ id: 7, name: "Rozšíření košíku", desc: "Je potřeba rozšířit košík o počet položek.", status: 0, userId: 4, projectId: 3 })
        .push({ id: 8, name: "Oprava v adminu", desc: "Oprava výpisu položek v administraci.", status: 0, userId: 5, projectId: 3 })
        .write();

    db.get('entries')
        // User ondra
        .push({ id: 1, desc: "Management projektu", userId: 1, projectId: 1, startTime: "2020-06-19T13:28:06.419Z", endTime: "2020-06-19T15:34:06.419Z" })
        .push({ id: 2, desc: "Management projektu", userId: 1, projectId: 1, startTime: "2020-06-21T12:16:06.419Z", endTime: "2020-06-21T13:42:06.419Z" })
        
        // User honza
        .push({ id: 3, desc: "Frontend - objednávky", userId: 2, projectId: 1, startTime: "2020-06-19T12:13:06.419Z", endTime: "2020-06-19T15:34:06.419Z" })
        .push({ id: 4, desc: "Frontend - uživatelé", userId: 2, projectId: 2, startTime: "2020-06-22T10:36:06.419Z", endTime: "2020-06-22T18:53:06.419Z" })
        .push({ id: 5, desc: "Frontend - login form", userId: 2, projectId: 2, startTime: "2020-06-23T08:23:06.419Z", endTime: "2020-06-23T15:48:06.419Z" })

        // User pepa
        .push({ id: 6, desc: "Backend - objednávky", userId: 3, projectId: 1, startTime: "2020-06-19T11:28:06.419Z", endTime: "2020-06-19T15:34:06.419Z" })
        .push({ id: 7, desc: "Backend - uživatelé", userId: 3, projectId: 2, startTime: "2020-06-25T15:36:06.419Z", endTime: "2020-06-25T18:23:06.419Z" })

        // User radim
        .push({ id: 8, desc: "Backend - sklad", userId: 4, projectId: 1, startTime: "2020-06-20T09:12:06.419Z", endTime: "2020-06-20T16:44:06.419Z" })
        .push({ id: 9, desc: "Backend - admin", userId: 4, projectId: 3, startTime: "2020-06-24T09:24:06.419Z", endTime: "2020-06-24T15:54:06.419Z" })

        // User jarda
        .push({ id: 10, desc: "Frontend - úvodní sekce", userId: 5, projectId: 3, startTime: "2020-06-20T11:38:06.419Z", endTime: "2020-06-20T16:47:06.419Z" })
  
        .write();


    // Default user
    db.get('options')
        .set('activeUser', 1)
        .write();
};

/**
 * Clear all data from database
 * @param {object} db - Takes database object.
 * @return - No return value.
 */
var clearData = function (db) {
    // Reset database
    db.set('users', [])
        .write();

    db.set('roles', [])
        .write();

    db.set('teams', [])
        .write();

    db.set('clients', [])
        .write();

    db.set('projects', [])
        .write();

    db.set('tasks', [])
        .write();

    db.set('entries', [])
        .write();

    db.set('timers', [])
        .write();

    // Default roles
    db.get('roles')
        .push({ id: 1, name: "Admin" })
        .push({ id: 2, name: "Developer" })
        .push({ id: 3, name: "Basic" })
        .write();

    // Default user
    db.get('options')
        .set('activeUser', 1)
        .write();
}


/** Export functions */
exports.loadData = loadData;
exports.clearData = clearData;