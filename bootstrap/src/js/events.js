/**
 * Copyright (c) 2020
 *
 * Events handling for Foundation demo app
 *
 * @author Ondrej Zeleny <ondrej.zeleny@live.com>
 */


/** Require libs */
const $ = require('jquery');
const parser = require('./parser.js');
const loadData = require('./load-data.js');
const helpers = require('./helpers.js');

/**
 * Bind events on DOM
 * @param {object} db - Takes database object.
 * @return - No return value.
 */
var bindEvents = function (db) {
    $( document ).ready(function() {

        // Nav open event
        $("#js-open-nav").click(function(e) {
            e.preventDefault();
            $(".wrapper").toggleClass("toggled");
        });

        // Redirect if no active user
        if(!(db.get('options.activeUser').value()) || parseInt(db.get('options.activeUser').value()) == 0) {
            var filename = window.location.pathname.split('/').pop()
            if (filename != "login.html") {
                window.location.replace("login.html");
            }
        }

        // Timer set
        var $timer = $("#js-timer-counter");
        if ($timer.length) {
            var seconds = 0;
            var timeString = $timer.text().split(":");
            var time;
            var finalString;

            seconds += parseInt(timeString[0]) * 60 * 60;
            seconds += parseInt(timeString[1]) * 60;
            seconds += parseInt(timeString[2]);

            setInterval(function(){ 
                seconds++;
                time = helpers.toTime(seconds * 1000);
                finalString = helpers.twoDigits(time.h) + ":" + helpers.twoDigits(time.m)+ ":" + helpers.twoDigits(time.s);
                $timer.text(finalString);
            }, 1000);    
        }

        // Set tooltip
        $('[data-toggle="tooltip"]').tooltip();

        // Load data
        $("#js-load-data").on( "click", function() {
            loadData.loadData(db);
            parser.parseTemplate(db);
        });

        // Logout button
        $("#js-logout").on( "click", function() {
            var options = db.get('options');
            options.set('activeUser', 0)
                .write();

            window.location.replace("login.html");
        });

        // Login form
        $("#js-login").submit(function( event ) {  
            event.preventDefault();
            var data = helpers.toObject($(this).serializeArray());
            var options = db.get('options');

            options.set('activeUser', parseInt(data.userId))
                .write();
                
            window.location.replace("index.html");
        });  

        // Add user
        $("#js-add-user").submit(function( event ) {  
            event.preventDefault();
            var data = helpers.toObject($(this).serializeArray());
            var users = db.get('users');
            var count = db.get('usersCount') + 1;

            db.set('usersCount', count).write();
            
            users.push({ id: count, username: data.username, name: data.name, roleId: parseInt(data.roleId), running: 0 })
                .write();

            $('.modal').modal('hide');
            parser.parseTemplate(db);
        });  
        
        // Add team
        $("#js-add-team").submit(function( event ) {  
            event.preventDefault();
            var data = helpers.toObject($(this).serializeArray());
            var teams = db.get('teams');
            var count = db.get('teamsCount') + 1;

            var users = [];
            $(this).serializeArray().forEach(function filter(value, index, array) {
                if (value.name == "users") {
                    users.push(parseInt(value.value));
                }
            })

            db.set('teamsCount', count).write();
            
            teams.push({ id: count, name: data.name, users: users, active: 1 })
                .write();

            $('.modal').modal('hide');
            parser.parseTemplate(db);
        });  

        // Delete team
        $("*[data-delete-team]").on( "click", function() {
            var id = $(this).data("delete-team");
            var teams = db.get('teams');

            teams.find({ id: parseInt(id) })
                .assign({ active: 0 })
                .write();

            parser.parseTemplate(db);
        });

        // Add client
        $("#js-add-client").submit(function( event ) {  
            event.preventDefault();
            var data = helpers.toObject($(this).serializeArray());
            var clients = db.get('clients');
            var count = db.get('clientsCount') + 1;

            db.set('clientsCount', count).write();
            
            clients.push({ id: count, name: data.name, desc: data.desc, active: 1 })
                .write();

            $('.modal').modal('hide');
            parser.parseTemplate(db);
        });  

        // Delete client
        $("*[data-delete-client]").on( "click", function() {
            var id = $(this).data("delete-client");
            var clients = db.get('clients');

            clients.find({ id: parseInt(id) })
                .assign({ active: 0 })
                .write()

            parser.parseTemplate(db);
        });

        // Add project
        $("#js-add-project").submit(function( event ) {  
            event.preventDefault();
            var data = helpers.toObject($(this).serializeArray());
            var projects = db.get('projects');
            var count = db.get('projectsCount') + 1;

            db.set('projectsCount', count).write();

            projects.push({ id: count, name: data.name, clientId: parseInt(data.clientId), teamId: parseInt(data.teamId), active: 1 })
                .write();

            $('.modal').modal('hide');
            parser.parseTemplate(db);
        }); 
        
        // Finish project
        $("*[data-finish-project]").on( "click", function() {
            var id = $(this).data("finish-project");
            var teams = db.get('projects');

            teams.find({ id: parseInt(id) })
                .assign({ active: 0 })
                .write()

            parser.parseTemplate(db);
        });

        // Finish task
        $("*[data-finish-task]").on( "click", function() {
            var id = $(this).data("finish-task");
            var tasks = db.get('tasks');

            tasks.find({ id: parseInt(id) })
                .assign({ status: 1 })
                .write()

            parser.parseTemplate(db);
        });

        // Delete task
        $("*[data-delete-task]").on("click", function() {
            var id = $(this).data("delete-task");
            var tasks = db.get('tasks');

            tasks.remove({ id: parseInt(id) })
                .write()

            parser.parseTemplate(db);
        });

        // Add task
        $("#js-add-task").submit(function( event ) {  
            event.preventDefault();
            var data = helpers.toObject($(this).serializeArray());
            var tasks = db.get('tasks');
            var count = db.get('tasksCount') + 1;

            db.set('tasksCount', count).write();

            tasks.push({ id: count, name: data.name, desc: data.desc, status: 0, userId: parseInt(data.userId), projectId: parseInt(data.projectId) })
                .write();

            $('.modal').modal('hide');
            parser.parseTemplate(db);
        }); 


        // Delete entry
        $("*[data-delete-entry]").on( "click", function() {
            var id = $(this).data("delete-entry");
            var entries = db.get('entries');

            entries.remove({ id: parseInt(id) })
                .write();

            parser.parseTemplate(db);
        });

        // Filter entry
        $("#js-entry-filter").change(function() {
            var value = parseInt($(this).val());
            var valTask = parseInt($("#js-task-filter").val());
             
            parser.parseTemplate(db, { taskFilter: parseInt(valTask), entryFilter: parseInt(value) });
        });
        

        // Filter tasks
        $("#js-task-filter").change(function() {
            var value = parseInt($(this).val());
            var valEntry = parseInt($("#js-entry-filter").val());
                
            parser.parseTemplate(db, { taskFilter: parseInt(value), entryFilter: parseInt(valEntry) });

        });

        // Start timer
        $("#js-timer-start").submit(function( event ) {  
            event.preventDefault();
            var data = helpers.toObject($(this).serializeArray());
            var timers = db.get('timers');
            var users = db.get('users');
            var count = db.get('timersCount') + 1;
            var activeUser = db.get('options.activeUser').value();

            db.set('timersCount', count).write();

            timers.push({ id: count, desc: data.desc, userId: activeUser, projectId: parseInt(data.projectId), startTime: Date(Date.now()) })
                .write();

            users.find({ id: activeUser })
                .assign({ running: count })
                .write()

            parser.parseTemplate(db);
        }); 

        // Stop timer
        $("#js-timer-stop").submit(function( event ) {  
            event.preventDefault();
            var timers = db.get('timers');
            var entries = db.get('entries');
            var users = db.get('users');
            var activeUser = db.get('options.activeUser').value();
            var activeUserObject = users.find({ id: activeUser });
            var runningTimer = activeUserObject.value().running;
            var count = db.get('entriesCount') + 1;

            db.set('entriesCount', count).write();

            var data = timers
                .find({ id: runningTimer })
                .value();

            entries.push({ id: count, desc: data.desc, userId: data.userId, projectId: data.projectId, startTime: data.startTime, endTime: Date(Date.now()) })
                .write();

            timers.remove({ id: runningTimer })
                .write();

            users.find({ id: activeUser })
                .assign({ running: 0 })
                .write();

            parser.parseTemplate(db);
        }); 

        // Reset timer
        $("#js-timer-stop").on('reset', function( event ) {  
            event.preventDefault();
            var timers = db.get('timers');
            var users = db.get('users');
            var activeUser = db.get('options.activeUser').value();
            var activeUserObject = users.find({ id: activeUser });
            var runningTimer = activeUserObject.value().running;

            timers.remove({ id: runningTimer })
                .write();

            users.find({ id: activeUser })
                .assign({ running: 0 })
                .write();

            parser.parseTemplate(db);
        }); 
    });
}


/** Export functions */
exports.bindEvents = bindEvents;